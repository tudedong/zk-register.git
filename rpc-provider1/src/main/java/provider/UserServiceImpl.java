package provider;

import com.tdd.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author tudedong
 * @description 服务端实现类
 * @date 2020-06-06 17:33:09
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * 提供给客户端调用的远程方法
     * @param msg
     * @return
     */
    public String sayHello(String msg) {
        System.out.println("are you ok ? "+msg);
        return "success";
    }
}
