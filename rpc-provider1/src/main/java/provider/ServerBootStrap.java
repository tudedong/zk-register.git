package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tudedong
 * @description ServerBootStrap会启动一个服务提供者，即NettyServer
 * @date 2020-06-06 17:49:05
 */
@SpringBootApplication
public class ServerBootStrap {

    public static void main(String[] args){
        SpringApplication.run(ServerBootStrap.class, args);
    }

}
