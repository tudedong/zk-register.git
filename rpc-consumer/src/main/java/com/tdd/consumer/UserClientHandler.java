package com.tdd.consumer;

import com.tdd.common.RpcRequest;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.Callable;

/**
 * @author tudedong
 * @description 客户端自定义事件处理器
 * 方法调用顺序：
 * channelActive() ==>setParam() ==>call() ==>channelRead() ==>call()
 * @date 2020-06-06 19:36:24
 */
@ChannelHandler.Sharable
public class UserClientHandler extends ChannelInboundHandlerAdapter implements Callable {

    /********1.定义成员变量,**********/
    /**
     * 事件处理器上下文对象，用来存储handler信息，进行写操作
     */
    private ChannelHandlerContext channelHandlerContext;

    /**
     * 记录服务器返回的数据
     */
    private String result;

    /**
     * 记录将要返回给服务器的数据
     */
    //private String param;
    private RpcRequest rpcRequest;

    /**
     * 2.实现 channelActive方法，客户端和服务端连接时，该方法自动执行
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //初始化ChannelHandlerContext
        channelHandlerContext = ctx;
    }

    /**
     * 3.实现channelRead 当我们读到服务器数据时，该方法自动执行
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //将读取到的服务器数据msg，赋值给成员变量
        result = msg.toString();
        //唤醒等待的线程
        notify();
    }

    /**
     * 4.被代理对象调用，将客户端数据写到服务器
     * @return
     * @throws Exception
     */
    public synchronized Object call() throws Exception {
        //channelHandlerContext给服务器写数据
        channelHandlerContext.writeAndFlush(rpcRequest);
        //进行wait，等待channelRead方法获得到服务器结果后，唤醒
        wait();
        return result;
    }

    /**
     * 5.设置参数param
     * @param rpcRequest
     */
    public void setRpcRequest(RpcRequest rpcRequest){
        this.rpcRequest = rpcRequest;
    }
}
