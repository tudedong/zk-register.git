package com.tdd.consumer;

import com.tdd.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tudedong
 * @description 客户端启动类
 * @date 2020-06-06 21:52:46
 */
@SpringBootApplication
public class ClientBootStrap {

    /**
     * 参数定义，即定义协议头
     */
    //private static final String PROVIDER_NAME = "UserService#sayHello#";

    public static void main(String[] args) throws InterruptedException {

        SpringApplication.run(ClientBootStrap.class, args);
        //1.创建代理对象
        UserService service = (UserService) NettyClient.createProxy(UserService.class);

        //2.循环给服务器写数据
        String result = service.sayHello("Netty&RPC");
        System.out.println("服务端返回结果：result:"+result);
//        while (true){
//            String result = service.sayHello("Netty&RPC");
//            System.out.println("服务端返回结果：result:"+result);
//            Thread.sleep(2000);
//        }

    }
}
