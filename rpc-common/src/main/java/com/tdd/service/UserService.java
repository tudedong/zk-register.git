package com.tdd.service;

/**
 * @author tudedong
 * @description 消费者和提供者约定的接口
 * @date 2020-06-06 13:28:48
 */
public interface UserService {

    /**
     * 消费者和提供者约定的接口的方法
     * @param msg
     * @return
     */
    public String sayHello(String msg);

}
