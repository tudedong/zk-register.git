package com.tdd.common;

import com.alibaba.fastjson.JSON;

/**
 * @author tudedong
 * @description 采用JSON的方式，定义JSONSerializer的实现类
 * @date 2020-06-06 22:37:58
 */
public class JSONSerializer implements Serializer{

    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}
