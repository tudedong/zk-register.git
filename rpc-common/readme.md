#基于netty实现自定义RPC


#思路步骤
1.创建项目
rpc-common
UserService（消费者和服务提供者约定的接口）
rpc-provider
ServerBoot（启动类：初始化远程服务端）
    ===> UserServiceHandler(服务端事件处理器：监听read操作)
        ===> UserServiceImpl（请求具体处理实现类）
rpc-consumer
启动类：初始化客户端，获取代理对象，发送请求
客户端事件处理器
（实现远程调用）jdk的动态代理，使用线程池分配一个线程处理自定义事件处理器中监听到的事件。